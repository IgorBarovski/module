class Cart {
    
    
    create() {
        const cartData = JSON.parse(localStorage.getItem('cart')) || []
        const cartElement = document.createElement('div');
        cartElement.classList.add('cart')

        if(cartData.length === 0) {
            cartElement.innerHTML=`<h2 class="cart__empty">Корзина пуста</h2>`
            return cartElement
        }
        

        let li= ''
        cartData.forEach(({image, count, price, title, id}) => {
        let amount =0
        amount+= count
         const buttonQ = document.createElement('button')
         buttonQ.classList.add('btn')
         console.log(buttonQ);

            li +=`<li class="cart__item">
            <div class="catalog__item__image">
                            <img src="${image}" alt="${title}">
                        </div>
                     <p>${title}</p>
                     <span>${count}шт.</span>
                     <p>${count*price} BYN</p>
                     <button id="${id}" class="cart__delete__button">Удалить</button>
              
                    
                  </li>
                  `
           
               
        });
        const amount = cartData.reduce((total,prod)=> total +prod.price,0)

        cartElement.innerHTML = `<ul class="cart_items">
                                    ${li}
                                    
                                    </ul>
                                    <button class="go__back__btn">Back to Catalog</button>
                                   <div class=total__price> Total price:${amount} BYN</div>
<button class="btn">Checkout</button>

                                                   `


         
    
  



        return cartElement;
    }
    
}

const cart = new Cart();
export default cart;