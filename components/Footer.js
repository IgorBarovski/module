class Footer {
    create (){
        const footerElement = document.createElement ('footer');
        footerElement.classList.add('footer');
        footerElement.innerHTML = ` <div class = "container">
                                                <div class = "footer__wrapper">
                                                    
                                                    <div class = "footer__logo">
                                                        <p>WHO WE ARE</p>
                                                        <a href = "#">
                                                            <img src= "https://sp-ao.shortpixel.ai/client/to_auto,q_glossy,ret_img,w_300,h_149/https://www.collo.fi/wp-content/uploads/2023/01/Collo_text_logo-300x149.png" width=80>
                                                        </a>
                                                    </div>


                                                    <div class = "footer__mobile">
                                                        <a href = "#">
                                                        <img src="image/AppStore.png" alt="">
                                                        <img src="image/Google Play.png" alt="">
                                                     
                                                        </a>
                                                    </div>

                                                    <div class = "footer__pay">
                                                        <a href = "#">
                                                            <img src= "image/MC.png" width=50>
                                                        </a>

                                                        <a href = "#">
                                                            <img src= "image/Visa.png" width=50>
                                                        </a>

                                                        <a href = "#">
                                                            <img src= "image/UP.png" width=50>
                                                        </a>
                                                    </div>
                                                    
                                                </div>
                                                <p class="footer__text">2004—2023 © Collo — модный интернет-магазин одежды, обуви
                                                и аксессуаров. Все права защищены.</p>
                                         </div>
                                    
                                    
                                    
                                    
                                    `
                                    

        return footerElement;
    }
}

const footer = new Footer();

export default footer;

