import { getDataFromLocalStorage } from '../service/dataFromLocalStorageService.js';
import { CATALOG } from '../constants/constants.js';
import { addToCart } from '../utils/utils.js';


class Main {
    #mainElement;

    addEventLocationChange () {
        window.addEventListener('hashchange', () => {
            this.render(location.hash)
        })
    }

    create (){
        this.#mainElement = document.createElement ('main');
        this.#mainElement.classList.add('main');
        
        this.render(location.hash)    
        this.addEventLocationChange()
        return this.#mainElement;
    }

    render(hash) {
        if(location.pathname === "/index.html" ||  (location.pathname === "/" &&  !location.hash)) {
          location.replace('/#home')
          
           return;
        }

   


        const slugFromHash = hash.slice(1);

        const  data = getDataFromLocalStorage();
        const mainData = data.find(({slug})=> slug === slugFromHash)

        if (slugFromHash === 'cart') {
            import('./Cart.js').then(responseCart => {
                const cartHTMLElement = responseCart.default.create()
                this.#mainElement.innerHTML = this.getHTMLTemplatePage(mainData.title, undefined, cartHTMLElement.outerHTML )

                const deleteButtons = this.#mainElement.querySelectorAll('.cart__delete__button');

                if(!deleteButtons){
                    return
                }


                deleteButtons.forEach((deleteButton) => {
                    deleteButton.addEventListener('click', (event)=>{
                        this.deleteCartProduct(+event.target.id);
                        this.render(location.hash);
                    })
                })
                const Buy = this.#mainElement.querySelectorAll('.btn')
                Buy.forEach(Buy=>{
                  Buy.addEventListener('click',(event)=>{
                        location.replace('/#buy')
                        
                    })
                   })
                   const goBackBtn = this.#mainElement.querySelector('.go__back__btn')
              
                    goBackBtn.addEventListener('click',()=>{
                      location.replace('/#catalog')
                    })


            })
            return
        }
        
        if (slugFromHash.includes(CATALOG)) {
           
            if(slugFromHash === CATALOG) {
                this.#mainElement.innerHTML = "<h2>Loading...</h2>"
                import('./Catalog.js').then((catalogResponse)=>{
                    const catalog = catalogResponse.default.init();
                    catalog.then((catalogHTMLElement)=>{
                        const catalogData = catalogResponse.default.catalogData;
                        this.#mainElement.innerHTML = this.getHTMLTemplatePage(mainData.title, undefined, catalogHTMLElement.outerHTML)
                        const addButtons = this.#mainElement.querySelectorAll('.catalog__item__add__cart')
                        addButtons.forEach(addButton => {
                            addButton.addEventListener('click', (event) => {
                                const product = catalogData.find(({id}) => id === +event.target.id)
                                addToCart(product)
                                
                            })



                            const clickBuy = this.#mainElement.querySelectorAll('.catalog__item__add__cart__buy')
                            clickBuy.forEach(clickBuy=>{
                              clickBuy.addEventListener('click',()=>{
                                    location.replace('/#buy')
                                    
                                })
                               })






                           
                        });
                    })
                })
                return;
            }

            this.#mainElement.innerHTML = "<h2>Loading...</h2>"
            import('./Product.js').then((responseProduct)=>{
                const product = responseProduct.default.init();
                product.then((productHTML) => {
                    const productData = responseProduct.default.productData;
                    this.#mainElement.innerHTML = productHTML.outerHTML;

                    const addButton = this.#mainElement.querySelector('.product__add__cart')

                    addButton.addEventListener('click', () => {
                        addToCart(productData)
                    })

                 
                })       
            })  
            return;       
        }
        if (slugFromHash === 'home') {
          this.#mainElement.innerHTML= ` 
          <div class="main__img">
            <div class="search">
                                <div class="icon"></div>
                    <div class="input">
                        <input class="input__search" type="text" placeholder="Search" id="mySearch">
                    </div>
        
            </div>
                <span class="clear"></span>
          </div>
      

   `
   const icon = this.#mainElement.querySelector('.icon')
   const search = this.#mainElement.querySelector('.search')

  icon.addEventListener('click',()=>{
    search.classList.toggle('active');
  })
   
 


          return
      }





        if (slugFromHash === 'contact') {
            this.#mainElement.innerHTML= ` <div class="container"><h2> We are in social networks</h2>
            <a  class="contacts__image"href="#"><img src="image/YouTube-Logo.png" alt="" width="80"></a>
            <a class="contacts__image" href="#"><img src="image/FB.png" alt="" width="80"></a>
            <a class="contacts__image" href="#"><img src="image/IN.jpg" alt="" width="80"></a></div>`



            return
        }

        if (slugFromHash === 'buy') {

            this.#mainElement.innerHTML= ` <div class="row">
            <div class="col-75">
              <div class="container">
                <form action="/action_page.php">
          
                  <div class="row">
                    <div class="col-50">
                    
                      <label for="fname"><i class="fa fa-user"></i> Full Name</label>
                      <input class="input__text" type="text" id="fname" name="firstname" placeholder="John M. Doe">
                      <label for="email"><i class="fa fa-envelope"></i> Email</label>
                      <input class="input__text"type="text" id="email" name="email" placeholder="john@example.com">
                      <label for="adr"><i class="fa fa-address-card-o"></i> Address</label>
                      <input class="input__text"type="text" id="adr" name="address" placeholder="Pr.Nezavisimisti 15">
                      <label for="city"><i class="fa fa-institution"></i> City</label>
                      <input class="input__text"type="text" id="city" name="city" placeholder="Minsk">
          
                      <div class="row">
                        <div class="col-50">
                         
                        
                        </div>
                        <div class="col-50">
                      
                        </div>
                      </div>
                    </div>
          
                    <div class="col-50">
                      <h3>Payment</h3>
                      
                 
                      <label for="cname">Name on Card</label>
                      <input class="input__text"type="text" id="cname" name="cardname" placeholder="John More Doe">
                      <label for="ccnum">Credit card number</label>
                      <input class="input__text"type="text" id="ccnum" name="cardnumber" placeholder="1111-2222-3333-4444">
                      <label for="expmonth">Exp Month</label>
                      <input class="input__text"type="text" id="expmonth" name="expmonth" placeholder="September">
          
                      <div class="row">
                        <div class="col-50">
                          <label for="expyear">Exp Year</label>
                          <input class="input__text" type="text" id="expyear" name="expyear" placeholder="2024">
                        </div>
                        <div class="col-50">
                          <label for="cvv">CVV</label>
                          <input class="input__text"type="text" id="cvv" name="cvv" placeholder="352">
                        </div>
                      </div>
                    </div>
          
                  </div>
                  <label>
                    <input type="checkbox" checked="checked" name="sameadr"> Shipping address same as billing
                  </label>
                  <input type="submit" value="Checkout" class="btn">
                </form>
              </div>
            </div>
          
           
          </div>  `
       
       

            return
        }
     
        this.#mainElement.innerHTML = this.getHTMLTemplatePage(mainData.title, mainData.content)
        
    }

    deleteCartProduct (productId) {
        const cart = JSON.parse(localStorage.getItem('cart'))
        const filterCart = cart.filter(({id}) => id !== productId)
        localStorage.setItem('cart', JSON.stringify(filterCart))
    }

    getHTMLTemplatePage (title, content, children) {
        return `<div class = "container">
                <div class = "main__wrapper">
                    <h1>${title}</h1>
                    
                    ${children ? children : ""}
                    ${content ? content : '' }
                </div>   
      
                
            </div>`
    }
}

const main = new Main();

export default main;